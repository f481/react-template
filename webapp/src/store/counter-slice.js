import {createSlice} from '@reduxjs/toolkit'

const counterSlice = createSlice(
{
    name: 'counter',
    initialState: {count:0},
    reducers:{
        increment(state){
            state.count++
        },
        decrement(state){
            state.count--
        },
        incrementByAmount(state,action){

            state.count = state.count +action.payload
        }
        

    }

}

)

export const actions = counterSlice.actions;
export default counterSlice;

