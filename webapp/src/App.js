import { Header, Form, Checkbox, Button } from "semantic-ui-react";
import Counter from "./components/Counter";
function App() {
  return (
    <div className="App">
      <Header>Hello world</Header>
      <Counter></Counter>
    </div>
  );
}

export default App;
