import React from "react";
import { Statistic, Button, ButtonGroup } from "semantic-ui-react";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { actions } from "../store/counter-slice";

function Counter() {
  const counts = useSelector((state) => state.counter.count);
  const dispatch = useDispatch();
  const increment = () => {
    dispatch(actions.increment());
  };
  const decrement = () => {
    dispatch(actions.decrement());
  };
  return (
    <div>
      <Statistic>
        <Statistic.Value>{counts}</Statistic.Value>
        <Statistic.Label>Downloads</Statistic.Label>
      </Statistic>
      <Button onClick={increment}>INC</Button>
      <Button onClick={decrement}>DEC</Button>
    </div>
  );
}

export default Counter;
